package com.example.empiktask.model;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "user")
@Getter
@Setter
public class User {

    @Id
    @Column(name = "login")
    private String login;

    @Column(name = "request_count")
    private Long requestCount;

}
