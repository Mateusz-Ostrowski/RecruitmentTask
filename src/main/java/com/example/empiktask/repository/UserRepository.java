package com.example.empiktask.repository;

import com.example.empiktask.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    @Modifying
    @Query(value = "INSERT INTO public.user (login, request_count) " +
            "VALUES (:login, 1) " +
            "ON CONFLICT (login) " +
            "DO UPDATE SET request_count = public.user.request_count + 1",
            nativeQuery = true)
    void saveOrIncrementRequestCountIfExists(String login);
}
