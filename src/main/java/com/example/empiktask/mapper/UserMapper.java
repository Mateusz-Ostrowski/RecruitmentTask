package com.example.empiktask.mapper;

import com.example.empiktask.dto.github.GithubUserDto;
import com.example.empiktask.dto.user.UserDto;
import com.example.empiktask.exception.user.UserCalculationException;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {
    public UserDto mapToUserDto(GithubUserDto user){
        return UserDto.builder()
                .id(user.getId())
                .type(user.getType())
                .login(user.getLogin())
                .name(user.getName())
                .avatarUrl(user.getAvatarUrl())
                .createdAt(user.getCreatedAt())
                .calculation(calculate(user.getFollowers(), user.getPublicRepos()))
                .build();
    }
    private Double calculate(Long followers, Long publicRepos){
        if(followers.equals(0L)){
            throw new UserCalculationException("Cant divide by 0 followers");
        }
        return (6.0 / followers * (2 + publicRepos));
    }
}
