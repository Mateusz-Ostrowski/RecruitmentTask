package com.example.empiktask.service.github;

import com.example.empiktask.dto.github.GithubUserDto;
import com.example.empiktask.exception.github.GithubApiCallLimitReachedException;
import com.example.empiktask.exception.github.GithubUserNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

@Slf4j
@RequiredArgsConstructor
public class GithubApiService {
    private final WebClient webClient;
    private final String url;
    public Mono<GithubUserDto> getUser(String login){
        log.info("Request GET {}/users/{}",url,login);

        return webClient.get()
                .uri(builder -> builder
                        .path("/users/{login}")
                        .build(login))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .onStatus(
                    code -> code.equals(HttpStatus.NOT_FOUND),
                    response -> response.bodyToMono(String.class)
                        .map(s -> new GithubUserNotFoundException(login)))
                .onStatus(
                    code -> code.equals(HttpStatus.FORBIDDEN),
                    response -> response.bodyToMono(GithubApiCallLimitReachedException.class))

                .bodyToMono(GithubUserDto.class);
    }

}
