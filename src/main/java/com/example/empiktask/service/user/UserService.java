package com.example.empiktask.service.user;

import com.example.empiktask.dto.github.GithubUserDto;
import com.example.empiktask.dto.user.UserDto;
import com.example.empiktask.mapper.UserMapper;
import com.example.empiktask.repository.UserRepository;
import com.example.empiktask.service.github.GithubApiService;
import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;
    private final UserMapper mapper;
    private final GithubApiService githubClient;

    @Transactional
    public UserDto getUserByLogin(String login){
        GithubUserDto githubUserDto = githubClient.getUser(login).block();
        userRepository.saveOrIncrementRequestCountIfExists(login);
        return mapper.mapToUserDto(githubUserDto);
    }

}
