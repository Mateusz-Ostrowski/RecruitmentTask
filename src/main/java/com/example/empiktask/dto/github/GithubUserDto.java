package com.example.empiktask.dto.github;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Builder;
import lombok.Data;

import java.time.Instant;
@Data
@Builder
@JsonDeserialize(builder =  GithubUserDto.GithubUserDtoBuilder.class)
public class GithubUserDto {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("login")
    private String login;

    @JsonProperty("name")
    private String name;

    @JsonProperty("type")
    private String type;

    @JsonProperty("followers")
    private Long followers;

    @JsonProperty("created_at")
    private Instant createdAt;

    @JsonProperty("public_repos")
    private Long publicRepos;

    @JsonProperty("avatar_url")
    private String avatarUrl;
}
