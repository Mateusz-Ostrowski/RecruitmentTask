package com.example.empiktask.controller;

import com.example.empiktask.dto.user.UserDto;
import com.example.empiktask.service.user.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/{login}")
    public ResponseEntity<UserDto> getUser(@PathVariable String login){
        UserDto result = userService.getUserByLogin(login);

        return ResponseEntity
                .ok(result);
    }
}
