package com.example.empiktask.config;

import com.example.empiktask.service.github.GithubApiService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.client.WebClient;

@Configuration
@Slf4j
public class GithubApiServiceConfiguration {
    @Bean
    public GithubApiService githubApiService(ApplicationProperties properties){
        log.info("Created GithubApiService with url {}",properties.getGithubApiUrl());
        return new GithubApiService(
                WebClient.builder()
                        .baseUrl(properties.getGithubApiUrl())
                        .build(),
                properties.getGithubApiUrl()
        );
    }
}
