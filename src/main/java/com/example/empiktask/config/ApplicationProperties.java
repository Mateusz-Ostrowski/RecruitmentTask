package com.example.empiktask.config;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("application")
@Getter
@Setter
public class ApplicationProperties {
    private String githubApiUrl;
}
