package com.example.empiktask.exception.github;

import com.example.empiktask.exception.common.ErrorDto;
import com.example.empiktask.exception.common.RestExceptionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class GithubExceptionHandler extends RestExceptionHandler {

    @ExceptionHandler(GithubApiCallLimitReachedException.class)
    @ResponseStatus(HttpStatus.TOO_MANY_REQUESTS)
    public ErrorDto apiCallLimitReached(GithubApiCallLimitReachedException ex){
        log.info("Exception: {}",ex.getMessage());
        return ErrorDto.builder()
                .message(ex.getMessage())
                .build();
    }

    @ExceptionHandler(GithubUserNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ErrorDto userNotFound(Exception ex){
        log.info("Exception: {}",ex.getMessage());
        return ErrorDto.builder()
                .message(ex.getMessage())
                .build();
    }

}
