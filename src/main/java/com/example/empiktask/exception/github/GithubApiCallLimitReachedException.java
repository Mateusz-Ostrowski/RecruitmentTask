package com.example.empiktask.exception.github;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GithubApiCallLimitReachedException extends RuntimeException {
    private String message;
}
