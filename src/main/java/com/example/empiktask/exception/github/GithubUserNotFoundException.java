package com.example.empiktask.exception.github;


public class GithubUserNotFoundException extends RuntimeException{

    public GithubUserNotFoundException(String login) {
        super(String.format("Not found github user for login: %s",login));
    }

}
