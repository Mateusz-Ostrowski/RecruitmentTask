package com.example.empiktask.exception.user;

import com.example.empiktask.exception.common.ErrorDto;
import com.example.empiktask.exception.common.RestExceptionHandler;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class UserExceptionHandler extends RestExceptionHandler {

    @ExceptionHandler(UserCalculationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorDto userCalculationException(Exception ex){
        return ErrorDto.builder()
                .message(ex.getMessage())
                .build();
    }
}
