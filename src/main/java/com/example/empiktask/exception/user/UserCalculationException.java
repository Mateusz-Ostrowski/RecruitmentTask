package com.example.empiktask.exception.user;

public class UserCalculationException extends RuntimeException{
    public UserCalculationException(String message){
        super(message);
    }
}
