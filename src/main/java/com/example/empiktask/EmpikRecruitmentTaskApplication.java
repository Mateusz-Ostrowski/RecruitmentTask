package com.example.empiktask;

import com.example.empiktask.config.ApplicationProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(ApplicationProperties.class)
public class EmpikRecruitmentTaskApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmpikRecruitmentTaskApplication.class, args);
    }

}
