package com.example.empiktask.mapper;

import com.example.empiktask.dto.github.GithubUserDto;
import com.example.empiktask.dto.user.UserDto;
import com.example.empiktask.exception.user.UserCalculationException;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Instant;

import static org.junit.jupiter.api.Assertions.*;

class UserMapperTest {
    private final Long ID = 1L;
    private final String LOGIN = "login";
    private final String TYPE = "type";
    private final String AVATAR_URL = "avatar_url";
    private final String NAME = "name";
    private final Instant CREATED_AT = Instant.now();
    private final Long PUBLIC_REPOS = 2L;
    private final Long FOLLOWERS = 20L;

    UserMapper mapper = new UserMapper();
    @Test
    public void shouldMapUser(){
        //Given
        GithubUserDto githubUserDto = GithubUserDto
                .builder()
                .id(ID)
                .login(LOGIN)
                .type(TYPE)
                .name(NAME)
                .avatarUrl(AVATAR_URL)
                .createdAt(CREATED_AT)
                .publicRepos(2L)
                .followers(20L)
                .build();


        //When
        UserDto result = mapper.mapToUserDto(githubUserDto);


        //Then
        Assertions.assertThat(result).isEqualTo(UserDto.builder()
                        .id(ID)
                        .login(LOGIN)
                        .name(NAME)
                        .type(TYPE)
                        .avatarUrl(AVATAR_URL)
                        .createdAt(CREATED_AT)
                        .calculation(6.0 / FOLLOWERS * (2 + PUBLIC_REPOS))
                        .build()
        );
    }

    @Test
    public void shouldThrowUserCalculationExceptionIfFollowerCountIs0(){
        //Given
        GithubUserDto githubUserDto = GithubUserDto
                .builder()
                .id(ID)
                .login(LOGIN)
                .type(TYPE)
                .name(NAME)
                .avatarUrl(AVATAR_URL)
                .createdAt(CREATED_AT)
                .publicRepos(PUBLIC_REPOS)
                .followers(0L)
                .build();

        //When/Then
        assertThrows(
                UserCalculationException.class,
                () -> mapper.mapToUserDto(githubUserDto)
        );

    }

}