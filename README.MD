# EmpikRecruitmentTask

### Uruchamianie aplikacji
```bash
./gradlew bootRun
```

### Uruchamianie kontenera z bazą postgresql
```bash
docker-compose -f src/main/docker/postgres.yml up
```
